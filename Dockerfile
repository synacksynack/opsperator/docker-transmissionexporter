FROM docker.io/golang:1.14 AS builder

# Transmission Exporter image for OpenShift Origin

WORKDIR /go/src/github.com/metalmatze/transmission-exporter

COPY config/cmd ./cmd
COPY config/*.go config/go.* ./

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && env GO15VENDOREXPERIMENT=1 \
	CGO_ENABLED=0 \
	GOOS=linux \
	go get ./ \
    && env GO15VENDOREXPERIMENT=1 \
	CGO_ENABLED=0 \
	GOOS=linux \
	go build -v ./cmd/transmission-exporter

FROM docker.io/alpine:latest

LABEL io.k8s.description="Transmission Prometheus Exporter Image." \
      io.k8s.display-name="Transmission Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,transmission" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-transmissionexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.0"

RUN set -x \
    && apk add --update ca-certificates

COPY --from=builder \
	/go/src/github.com/metalmatze/transmission-exporter/transmission-exporter \
	/transmission-exporter

ENTRYPOINT ["/transmission-exporter"]
USER 1001
